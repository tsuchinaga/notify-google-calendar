import CalendarEventUpdated = GoogleAppsScript.Events.CalendarEventUpdated;

function main() {
    const calID = PropertiesService.getScriptProperties().getProperty("CALENDAR_ID")
    let cal = CalendarApp.getCalendarById(calID)

    let start = new Date()
    let end = new Date()
    end.setDate(end.getDate() + 7)
    let events = cal.getEvents(start, end)

    let embeds = []
    for (let event of events) {
        embeds.push({"title": Utilities.formatDate(event.getStartTime(), "JST", "yyyy/MM/dd (E) HH:mm") + " " + event.getTitle()})
    }
    if (embeds.length > 0) post("先7日間の予定", embeds)
}

function update(e: CalendarEventUpdated) {
    const syncToken = PropertiesService.getScriptProperties().getProperty("SYNC_TOKEN")

    let args = {"showDeleted": true}
    if (syncToken !== "") args["syncToken"] = syncToken

    let events = Calendar.Events.list(e.calendarId, args)
    PropertiesService.getScriptProperties().setProperty("SYNC_TOKEN", events.nextSyncToken)
    if (syncToken === "") return // 初回のカレンダー更新は直前の状態を保持できていないので通知しない

    let items = events.items
    let embeds = []
    for (let i = 0; i < items.length; i++) {
        let item = items[i]
        if (item.status === "cancelled") continue

        let datetime = item.start.date
        if (datetime == null) datetime = item.start.dateTime

        embeds.push({"title": Utilities.formatDate(new Date(datetime), "JST", "yyyy/MM/dd (E) HH:mm") + " " + item.summary})
    }
    if (embeds.length > 0) post("更新された予定", embeds)
}

function post(title: string, embeds: object) {
    let url = PropertiesService.getScriptProperties().getProperty("DISCORD_WEBHOOK")
    if (url === null || url === "") {
        Logger.log("DISCORD_WEBHOOKが設定されていません")
        return
    }
    UrlFetchApp.fetch(url, {
        "method": "post",
        "contentType": "application/json",
        "payload": JSON.stringify({"content": title, "embeds": embeds})
    })
}
